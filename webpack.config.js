const autoprefixer = require('autoprefixer');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');

const cssLoaders = ['style', 'css', 'postcss-loader'];
var styleModLoaders = [
    { test: /\.scss$/, loaders: cssLoaders.concat([
        "sass?precision=10&outputStyle=expanded&sourceMap=true&includePaths[]=" + path.resolve(__dirname, './src')]) },
    { test: /\.css$/ , loaders: cssLoaders }
];
styleModLoaders = styleModLoaders.map( function(e) {
    return {
        test: e.test, 
        loader: ExtractTextPlugin.extract(e.loaders.slice(1).join('!'))
    }
});

const jsLoaders = [/\.jsx$/, /\.js$/].map(function (template) {
    return {
        test: template,
        exclude: /node_modules/,
        loaders: ['babel-loader']
    }
});

const otherLoaders = [/\.jpg$/, /\.png$/].map(function (template) {
    return {
        test: template,
        loader: 'file-loader?name=/[name].[ext]'
    }
});

const config = {
    entry: {
        app: ['./src/index.jsx']
    },
    module: {
        loaders: jsLoaders.concat(styleModLoaders).concat(otherLoaders)
    },
    output: {
        filename: '[name].js',
        path: path.join(__dirname, './webroot/static'),
        publicPath: '/static'
    },
    plugins: [
        new ExtractTextPlugin('[name].css')
    ],
    postcss: [
        autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9']
        })
    ],
    resolve: {
        extensions: ['', '.js', '.jsx', '.scss'],
        root: [path.join(__dirname, './src')]
    }
};

module.exports = config;