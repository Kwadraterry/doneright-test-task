import React from 'react'
import './styles/testimonial.scss'
import jQuery from 'jquery'

export default class Testimonial extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            auxiliaryTextVisible: false
        };

        this.showAuxiliaryText = this.showAuxiliaryText.bind(this);
    }

    showAuxiliaryText() {
        jQuery(this._auxiliaryText).slideDown(()=>{this.setState({ auxiliaryTextVisible: !this.state.auxiliaryTextVisible })});        
    }
    
    getExpandAuxiliaryClasses() {
        return "testimonial__expand-auxiliary" + (this.state.auxiliaryTextVisible?" testimonial__expand-auxiliary_minus":" testimonial__expand-auxiliary_plus")
    }
    
    render() {
        return (
            <div className="testimonial" >
                <div className="testimonial__author">
                    <img src="images/face1.png" alt="" className="testimonial__face"/>
                    <img src="images/face_outline.png" alt="" className="testimonial__stroke"/>
                    <div className="testimonial__background-direct">&nbsp;</div>
                    <img src="images/bg_irregular_shape.png" alt="" className="testimonial__background-shape"/>
                </div>
                <div className="testimonial__left-quote">
                    <img src="images/quote_icon_start.png" alt="" className="testimonial__left-quote-img"/>
                </div>
                <div className="testimonial__empty-space">
                </div>
                <div className="testimonial__right-quote">
                    <img src="images/quote_icon_end.png" alt="" className="testimonial__right-quote-img"/>
                </div>
                <div className="testimonial__citation-text testimonial__citation-text_main">
                    <p className="testimonial__citation-main-paragraph">
                        {this.props.textMain}
                    </p>
                </div>
                <div className="testimonial__citation-text testimonial__citation-text_auxiliary" ref={(c) => this._auxiliaryText = c}>
                    {this.props.textAuxiliary.map((paragraph, i) =>
                        <p key={i} className={"testimonial__citation-auxiliary-paragraph testimonial__citation-auxiliary-paragraph-" + i}>{paragraph}</p>
                    )}
                </div>
                <button onClick={this.showAuxiliaryText} className={this.getExpandAuxiliaryClasses()}>
                </button>
                <div className="testimonial__author-name"><span className="testimonial__author-name-dash">&mdash;</span> {this.props.name},
                    <span>{this.props.position}</span><span className="testimonial__author-name-dash">&mdash;</span>
                </div>
            </div>
        )
    }
}

Testimonial.propTypes = {
    photo: React.PropTypes.string.isRequired,
    name: React.PropTypes.string.isRequired,
    position: React.PropTypes.string.isRequired,
    textMain: React.PropTypes.string.isRequired,
    textAuxiliary: React.PropTypes.arrayOf(React.PropTypes.node).isRequired
};