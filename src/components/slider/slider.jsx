import React from 'react'
import './styles/slider.scss'
import $ from 'jquery'

export default class Slider extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            currentSelection:0
        };
        this._count = this.props.children.length;
        this._lis = {};

        this.fingerCount = 0;
        this.startX = 0;
        this.startY = 0;
        this.curX = 0;
        this.curY = 0;
        this.deltaX = 0;
        this.deltaY = 0;
        // this.horzDiff = 0;
        // this.vertDiff = 0;
        this.minLength = 72;
        this.swipeLength = 0;
        this.swipeAngle = null;
        this.swipeDirection = null;
        
        this.goToNext = this.goToNext.bind(this);
        this.goToPrevious = this.goToPrevious.bind(this);
        this.caluculateAngle = this.caluculateAngle.bind(this);
        this.determineSwipeDirection = this.determineSwipeDirection.bind(this);
        this.processingRoutine = this.processingRoutine.bind(this);
        this.slideTo = this.slideTo.bind(this);
        this.touchCancel = this.touchCancel.bind(this);
        this.touchEnd = this.touchEnd.bind(this);
        this.touchMove = this.touchMove.bind(this);
        this.touchStart = this.touchStart.bind(this);

    }
    generateDots(){
        const dotClasses = (num) => "slider__dot" + ((num===this.state.currentSelection)?" slider__dot_active":"");
        var dots = [];
        for (var i = 0; i < this._count; i++){
            const dotNumber = i;
            dots.push(
                <span key={i} className={dotClasses(dotNumber)} onClick={()=>{this.slideTo(dotNumber)}}>&nbsp;</span>
            )
        }
        return dots;
    }    
    slideTo(i) {
        var step = $(this._lis[i]).width(); // ширина объекта 
        $(this._ul).animate({marginLeft: "-"+step*i}, 500); // 500 это скорость перемотки
        this.setState({currentSelection:i})
    }
    goToNext(){
        const current = this.state.currentSelection;
        if (current < this._count-1){
            this.slideTo(current+1)
        }
        else{
            this.slideTo(0)
        }
    }
    goToPrevious(){
        const current = this.state.currentSelection;
        if (current > 0) {
            this.slideTo(current - 1)
        } else {
            this.slideTo(this._count - 1)
        }
    }
    touchStart(event) {
        // disable the standard ability to select the touched object
        event.preventDefault();
        // get the total number of fingers touching the screen
        this.fingerCount = event.touches.length;
        // since we're looking for a swipe (single finger) and not a gesture (multiple fingers),
        // check that only one finger was used
        if ( this.fingerCount == 1 ) {
            // get the coordinates of the touch
            this.startX = event.touches[0].pageX;
            this.startY = event.touches[0].pageY;
            // store the triggering element ID
            // triggerElementID = passedName;
        } else {
            // more than one finger touched so cancel
            this.touchCancel(event);
        }
    }

    touchMove(event) {
        event.preventDefault();
        if ( event.touches.length == 1 ) {
            this.curX = event.touches[0].pageX;
            this.curY = event.touches[0].pageY;
        } else {
            this.touchCancel(event);
        }
    }

    touchEnd(event) {
        event.preventDefault();
        // check to see if more than one finger was used and that there is an ending coordinate
        if ( this.fingerCount == 1 && this.curX != 0 ) {
            // use the Distance Formula to determine the length of the swipe
            this.swipeLength = Math.round(Math.sqrt(Math.pow(this.curX - this.startX,2) + Math.pow(this.curY - this.startY,2)));
            // if the user swiped more than the minimum length, perform the appropriate action
            if ( this.swipeLength >= this.minLength ) {
                this.caluculateAngle();
                this.determineSwipeDirection();
                this.processingRoutine();
                this.touchCancel(event); // reset the variables
            } else {
                this.touchCancel(event);
            }
        } else {
            this.touchCancel(event);
        }
    }

    touchCancel(event) {
        // reset the variables back to default values
        this.fingerCount = 0;
        this.startX = 0;
        this.startY = 0;
        this.curX = 0;
        this.curY = 0;
        this.deltaX = 0;
        this.deltaY = 0;
        // this.horzDiff = 0;
        // this.vertDiff = 0;
        this.swipeLength = 0;
        this.swipeAngle = null;
        this.swipeDirection = null;
    }

    caluculateAngle() {
        var X = this.startX-this.curX;
        var Y = this.curY-this.startY;
        var Z = Math.round(Math.sqrt(Math.pow(X,2)+Math.pow(Y,2))); //the distance - rounded - in pixels
        var r = Math.atan2(Y,X); //angle in radians (Cartesian system)
        this.swipeAngle = Math.round(r*180/Math.PI); //angle in degrees
        if ( this.swipeAngle < 0 ) { this.swipeAngle =  360 - Math.abs(this.swipeAngle); }
    }

    determineSwipeDirection() {
        if ( (this.swipeAngle <= 45) && (this.swipeAngle >= 0) ) {
            this.swipeDirection = 'left';
        } else if ( (this.swipeAngle <= 360) && (this.swipeAngle >= 315) ) {
            this.swipeDirection = 'left';
        } else if ( (this.swipeAngle >= 135) && (this.swipeAngle <= 225) ) {
            this.swipeDirection = 'right';
        } else if ( (this.swipeAngle > 45) && (this.swipeAngle < 135) ) {
            this.swipeDirection = 'down';
        } else {
            this.swipeDirection = 'up';
        }
    }

    processingRoutine() {
        var swipedElement = this._ul;
        if ( this.swipeDirection == 'left' ) {
            this.goToNext();
        } else if ( this.swipeDirection == 'right' ) {
            this.goToPrevious();
        } 
    }

    render() {
        return (
            <div className="slider">
                <button className="slider__arrow-button slider__arrow-button_right" onClick={this.goToNext}>
                    <img src="images/arrow.svg" alt="" className="slider__arrow-right-img"/>
                </button>
                <button className="slider__arrow-button slider__arrow-button_left" onClick={this.goToPrevious}>
                    <img src="images/arrow.svg" alt="" className="slider__arrow-left-img"/>
                </button>
                <div className="slider__items">
                    <ul className="slider__container" ref={(c)=>{this._ul=c}} id="swipeBox" onTouchStart={this.touchStart} onTouchEnd={this.touchEnd} onTouchMove={this.touchMove} onTouchCancel={this.touchCancel}>
                        {this.props.children.map((child,i)=>(
                            <li key={i} className="slider__element" ref={(c)=>{this._lis[i]=c}}>
                                {child}
                            </li>
                        ))}
                    </ul>    
                </div>
                <div className="slider__dots">
                    {this.generateDots()}
                </div>
            </div>
        )
    }
}