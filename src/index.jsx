import './common/normalize.css'
import './common/main.scss'
import './components/top-menu/top-menu.scss'

import React from 'react'
import ReactDOM from 'react-dom'
import Testimonial from './components/testimonial/testimonital'
import Slider from './components/slider/slider'

const loremIpsum = "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.";

ReactDOM.render(
    <Slider>
        <Testimonial
            photo="images/face.png"
            name="John Smith"
            position="Commercial Director, Boeing"
            textMain={loremIpsum}
            textAuxiliary={[loremIpsum, loremIpsum, loremIpsum]}>
        </Testimonial>
        <Testimonial
            photo="images/face.png"
            name="John Smith"
            position="Commercial Director, Boeing"
            textMain={loremIpsum}
            textAuxiliary={[loremIpsum, loremIpsum, loremIpsum]}>
        </Testimonial>
        <Testimonial
            photo="images/face.png"
            name="John Smith"
            position="Commercial Director, Boeing"
            textMain={loremIpsum}
            textAuxiliary={[loremIpsum, loremIpsum, loremIpsum]}>
        </Testimonial>
        <Testimonial
            photo="images/face.png"
            name="John Smith"
            position="Commercial Director, Boeing"
            textMain={loremIpsum}
            textAuxiliary={[loremIpsum, loremIpsum, loremIpsum]}>
        </Testimonial>
    </Slider>
    , document.querySelector('#main-slider'));